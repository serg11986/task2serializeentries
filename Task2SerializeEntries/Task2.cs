﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using static Task2SerializeEntries.Helper;

namespace Task2SerializeEntries
{

    public static class Helper
    {
        public static bool IsNullOrEmpty<T>(IEnumerable<T> sequence) => sequence == null || !sequence.Any();
    }

    public class Entry
    {
        public string Name { get; set; }

        public List<string> Values { get; set; }

        public override string ToString()
        {
            return $"{Name} : {(IsNullOrEmpty(Values) ? "[]" : $"[{string.Join(", ", Values)}]")}";
        }
    }

    class Program
    {

        

        public static string Serialize(List<Entry> entries)
        {
            var dict = new Dictionary<string, object>();
            foreach (Entry entry in entries)
            {
                dict[entry.Name] = IsNullOrEmpty(entry.Values) ? null : (entry.Values.Count == 1 ? (object)$"{entry.Values[0]}" : entry.Values);
            }
            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }

        public static List<Entry> Deserialize(string json)
        {
            List<Entry> entries = new List<Entry>();
            var dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            foreach (var kvp in dict)
            {
                var entry = new Entry { Name = kvp.Key };
                if (kvp.Value != null)
                {
                    if (kvp.Value is string s)
                        entry.Values = new List<string> { s };
                    else if (kvp.Value is Newtonsoft.Json.Linq.JArray list)
                        entry.Values = list.ToObject<List<string>>();
                }
                entries.Add(entry);
            }
            return entries;
        }

        static void Main(string[] args)
        {
            List<Entry> entries = new List<Entry>
            {
                new Entry { Name = "nullEntry", Values = null},
                new Entry { Name = "emptyEntry", Values = new List<string>() },
                new Entry { Name = "singleEntry1", Values = new List<string> { "singleValue1" } },
                new Entry { Name = "singleEntry2", Values = new List<string> { "singleValue2" } },
                new Entry { Name = "multipleEntry1", Values = new List<string> { "multipleValue1-1", "multipleValue1-2", "multipleValue1-3" } },
                new Entry { Name = "multipleEntry2", Values = new List<string> { "multipleValue2-1", "multipleValue2-2", "multipleValue2-3" } }
            };

            string json = Serialize(entries);
            Console.WriteLine("СЕРИАЛИЗОВАННЫЕ ЗАПИСИ:");
            Console.WriteLine(json);
            List<Entry> entriesFromJson = Deserialize(json);
            Console.WriteLine("ДЕСЕРИАЛИЗОВАННЫЕ ЗАПИСИ:");
            foreach (Entry entry in entriesFromJson)
            {
                Console.WriteLine(entry);
            }
            Console.ReadLine();
        }
    }
}
